pragma solidity ^0.4.24;

contract Banking {

    uint public etherValue = 1000000000000000000;
    mapping(address => uint) public userBalance;
    
    event withdrawalProcess(address user, uint amount);
    event depositionProcess(address user, uint amount);

    function deposition() public payable {
        require(msg.value > 0);
        userBalance[msg.sender] += msg.value;
        emit depositionProcess(msg.sender, msg.value);
    }

    function withdrawal(uint _requiredAmount) public {
        require (_requiredAmount < userBalance[msg.sender]);
        userBalance[msg.sender] -= _requiredAmount;
        msg.sender.send(_requiredAmount);
        emit withdrawalProcess(msg.sender, _requiredAmount);
    } 
}